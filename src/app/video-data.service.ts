import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

interface ViewDetail {
  age: number;
  region: string;
  date: string;
}

export interface Video {
  title: string;
  author: string;
  id: string;
  viewDetails: ViewDetail[];
}

function uppercaseAuthorNames(videos: Video[]) {
  return videos.map(v => ({ ...v, author: v.author.toUpperCase() }));
}

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  currentVideo = new BehaviorSubject<Video>(null);

  constructor(private http: HttpClient) { }

  setCurrentVideo(v: Video) {
    this.currentVideo.next(v);
  }

  loadVideos(): Observable<Video[]> {
    return this.http
      .get<Video[]>('https://api.angularbootcamp.com/videos')
      .pipe(map(videos => uppercaseAuthorNames(videos)))
    ;
  }

  loadVideoDetails(id): Observable<Video> {
    return this.http
      .get<Video>('https://api.angularbootcamp.com/videos?id=' + id)
      .pipe(
        tap(data => {
          console.table('Video details:', data);
        }),
        map(videos => videos[0])
      )
    ;
  }
}
