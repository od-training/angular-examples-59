import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { Video, VideoDataService } from '../video-data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  videos: Observable<Video[]>;
  
  constructor(svc: VideoDataService) {
    this.videos = svc.loadVideos();
  }
}
