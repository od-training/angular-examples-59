import { Component, Input } from '@angular/core';

import { Video, VideoDataService } from '../../video-data.service';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent {
  @Input() videoData: Video[];

  constructor(public svc: VideoDataService) { }
}
